# New machine setup for linux (and windows) stuff.

Step 1: install emacs
```
sudo apt-get install emacs24 git ess
```

Step 2: pull this repo (at ~/)
```
git clone https://bitbucket.org/thashim/dotemacs.git
```

Step 3: link the dotfiles
```
ln -s ~/dotemacs/.emacs.linux ~/.emacs
```

Step 4: start emacs-nw, install color-theme-sanityinc-solarized-dark

Step 5: Install solarized
```
git clone https://github.com/sigurdga/gnome-terminal-colors-solarized.git
cd gnome-terminal-colors-solarized
./install.sh
```

# Programming environment related

Step 1: install anaconda (from https://www.continuum.io/downloads)
```
bash ~/Downloads/Anaconda2*.sh
```

Step 2: verify jupyter notebook works
```
jupyter notebook
```

# Stanford related

Install kerberos
```
sudo apt-get install krb5-user
wget https://support.cs.stanford.edu/hc/en-us/article_attachments/203383645/krb5.conf
sudo cp krb5.conf /etc/krb5.conf
```

Install afs
```
sudo apt-get install openafs-krb5 openafs-client krb5-user module-assistant openafs-modules-dkms
sudo /etc/init.d/openafs-client restart
ln -s /afs/cs.stanford.edu/u/thashim ~/afs
```