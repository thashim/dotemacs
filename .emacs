(when (>= emacs-major-version 24)
  (require 'package)
  (add-to-list 'package-archives '("elpa" . "http://elpa.gnu.org/") t)
  (add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
)

(setq package-enable-at-startup nil)
(package-initialize)

(require 'ess-site)
(setq ess-eval-visibly-p nil)

(require 'color-theme)
    (color-theme-initialize)
    (color-theme-arjen)

(require 'tex-site)
(setq-default TeX-master nil)

(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq indent-line-function 'insert-tab)

(require 'window-number)
(window-number-mode)
(window-number-meta-mode)

(setq-default show-trailing-whitespace t)
(add-hook 'before-save-hook 'delete-trailing-whitespace)
(setq truncate-partial-width-windows nil)

(custom-set-variables
 '(ispell-program-name "aspell"))